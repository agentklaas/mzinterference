#include <QApplication>
#include <QWidget>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QVector>

#include <iostream>
#include <vector>
#include <cmath>

#include "qcustomplot.h"

#include "Sidebar.h"

#define PI 3.14159265

class MZInterference : public QWidget
{
	Q_OBJECT

private:
	std::vector<double> d,n;
	double wavelength; //in vacuum
	double diameter, divergence;
	double s2i; //source to interferometer distance for pointsource approx 

	double pr;
	int numPoints;

	std::vector<std::vector<double> > intensity;

	QGridLayout* glayout;
	Sidebar* sidebar;
	QCustomPlot* signal;
	QCPColorMap* colormap;

	void connectAll();

public:
	MZInterference(QWidget* parent=nullptr);
	QSize sizeHint() const override;

	void init1();
	void init2();
	void refresh();
	
	void generatePosition();
	void generateIntensity();

public slots:
	void setD0(double in);
	void setN0(double in);
	void setD1(double in);
	void setN1(double in);
};
