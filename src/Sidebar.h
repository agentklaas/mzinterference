#pragma once

#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QDoubleSpinBox>
#include <QLabel>

//#include "QScienceSpinBox.h"
#include "QController.h"

class Sidebar : public QWidget
{
	private:
	QVBoxLayout* vlayout;

	QController *d0, *n0;
	QController *d1, *n1;

	public:
	Sidebar(QWidget* parent);
	QSize sizeHint() const override;

	QController* getD0();
	QController* getN0();
	QController* getD1();
	QController* getN1();
};
