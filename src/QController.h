#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QSlider>
#include <QDoubleSpinBox>
#include <limits>

class QController: public QWidget
{
	Q_OBJECT

private:
	double value;
	double min,max;

	int sliderMin, sliderMax;

	QGridLayout* glayout;
	QLabel* label;
	QSlider* slider;
	QDoubleSpinBox* box;

	virtual void arrange();

public:
	QController(QWidget* parent, QString inTitle, double inValue=0.0, double inMin=0.0, double inMax=1.0);

	void setRange(double inMin, double inMax);
	QLabel* getLabel();
	QSlider* getSlider();
	QDoubleSpinBox* getBox();


signals:
	void valueChanged(double newValue);


private slots:
	void setValue_box(double in);
	void setValue_slider(int in);

public slots:
	void setValue(double in);
};
