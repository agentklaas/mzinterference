#include <QApplication>
#include <QHBoxLayout>

#include <iostream>
#include <vector>

#include "MZInterference.h"

int main(int argc, char **argv)
{
	//set some defaults
	QLocale::setDefault(QLocale::C);//set default Language(and thus number format) to english
	//prepare OpenGL use for VTK
	//QSurfaceFormat format = QVTKOpenGLWidget::defaultFormat();
	//QSurfaceFormat::setDefaultFormat(format);

	//create the main window
	QApplication* app = new QApplication(argc, argv);
	MZInterference* mzinterference = new MZInterference();

	//actually start the application and enter mainloop
 	mzinterference->show();
	int returnval = app->exec();

	//delete app
	delete app;
 	return returnval;
}
