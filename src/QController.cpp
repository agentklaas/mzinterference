#include "QController.h"

QController::QController(QWidget* parent, QString inTitle, double inValue, double inMin, double inMax) : QWidget(parent)
{

	glayout = new QGridLayout(this);
	label = new QLabel(inTitle, this);
	slider = new QSlider(Qt::Horizontal, this);
	box = new QDoubleSpinBox(this);

	sliderMin = std::numeric_limits<int>::min();
	sliderMax = std::numeric_limits<int>::max();

	slider->setMinimum(sliderMin);
	slider->setMaximum(sliderMax);

	setRange(inMin, inMax);
	setValue(inValue);

	QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(setValue_slider(int)));
	QObject::connect(box, SIGNAL(valueChanged(double)), this, SLOT(setValue_box(double)));

	this->arrange();
}

void QController::arrange()
{
	glayout->addWidget(label, 0,0);
	glayout->addWidget(slider,0,1);
	glayout->addWidget(box,0,2);
}

void QController::setRange(double inMin, double inMax)
{
	min = inMin;
	max = inMax;
	
	box->setRange(min, max);
}

void QController::setValue_slider(int in)
{
	double percentage = (double)((long)in-(long)sliderMin)/(double)((long)sliderMax-(long)sliderMin);
	double newValue = percentage*(max-min)+min;
	
	if(newValue != value)
	{
		emit valueChanged(newValue);
	}
	box->setValue(newValue);
}

void QController::setValue_box(double in)
{
	if(in != value)
	{
		value = in;
		double percentage = (in-min)/(max-min);
		double newSliderValue = (int)((long)sliderMin+((long)sliderMax-(long)sliderMin)*percentage);
		slider->setValue(newSliderValue);

		emit valueChanged(in);
	}
}

void QController::setValue(double in)
{
	if(in<min)
	{
		in = min;
	}
	else if(in>max)
	{
		in = max;
	}
	setValue_box(in);
}


QLabel* QController::getLabel()
{
	return label;
}

QSlider* QController::getSlider()
{
	return slider;
}

QDoubleSpinBox* QController::getBox()
{
	return box;
}
