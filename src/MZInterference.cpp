#include "MZInterference.h"

MZInterference::MZInterference(QWidget* parent) : QWidget(parent)
{
	//Horizontal Layout would also do the job in this case
	glayout = new QGridLayout(this);
	glayout->setContentsMargins(0, 0, 0, 0); //Left Top Right Bottom
	glayout->setSpacing(0);
	this->setLayout(glayout);

	sidebar = new Sidebar(this);
	sidebar->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
	glayout->addWidget(sidebar,0, 0, -1, 1);

	signal = new QCustomPlot(this);
	signal->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	glayout->addWidget(signal, 0, 1);

	init1();

	colormap = new QCPColorMap(signal->xAxis, signal->yAxis);
	colormap->data()->setSize(numPoints, numPoints);
	colormap->data()->setRange(QCPRange(-1.0*pr, pr), QCPRange(-1.0*pr, pr));
	colormap->rescaleAxes();//maybe after rescaling data instead
	//colormap->setGradient(QCPColorGradient::gpPolar);
	//colormap->rescaleDataRange();

	generateIntensity();
	refresh();

	connectAll();
	init2();
}

void MZInterference::connectAll()
{
	QObject::connect(sidebar->getD0(), SIGNAL(valueChanged(double)), this, SLOT(setD0(double)));
	QObject::connect(sidebar->getN0(), SIGNAL(valueChanged(double)), this, SLOT(setN0(double)));
	QObject::connect(sidebar->getD1(), SIGNAL(valueChanged(double)), this, SLOT(setD1(double)));
	QObject::connect(sidebar->getN1(), SIGNAL(valueChanged(double)), this, SLOT(setN1(double)));
}

QSize MZInterference::sizeHint() const
{
	return QSize(1000,800);
}

void MZInterference::init1()
{
	pr = 1.0; //physical size of xmax
	wavelength = 1.0e-6;
	numPoints = 200;
	s2i = 1.0e1;
	d.push_back(1.0e-1);
	n.push_back(1.0);
	d.push_back(1.0e-1);
	n.push_back(1.2);
}

void MZInterference::init2()
{
	sidebar->getD0()->setRange(0.0, 1.0);
	sidebar->getD0()->setValue(1.0e-1);
	sidebar->getN0()->setRange(0.0, 2.0);
	sidebar->getN0()->setValue(1.0);
	sidebar->getD1()->setRange(0.0, 1.0);
	sidebar->getD1()->setValue(1.0e-1);
	sidebar->getN1()->setRange(0.0, 2.0);
	sidebar->getN1()->setValue(1.0);
}

void MZInterference::refresh()
{
	for(int i=0; i<intensity.size(); i++)
	{
		double x = -1.0*pr + (2.0*pr/(numPoints))*i; 
		for(int j=0; j<intensity[i].size(); j++)
		{
			double y = -1.0*pr + (2.0*pr/(numPoints))*j; 
			colormap->data()->cellToCoord(i,j,&x,&y);
			colormap->data()->setCell(i,j,intensity[i][j]);
		}
	}
	colormap->rescaleDataRange();
	signal->replot();
}

void MZInterference::generateIntensity()
{
	intensity.clear();
	for(int i=0; i<numPoints; i++)
	{
		double x = -1.0*pr + (2.0*pr/(numPoints-1))*i; //put points onto edges of range

		intensity.push_back(std::vector<double>());
		for(int j=0; j<numPoints; j++)
		{
			double y = -1.0*pr + (2.0*pr/(numPoints-1))*j; //put points onto edges of range

			std::vector<double> E{0.0, 0.0}; 
			for(int k=0; k<d.size(); k++)
			{
				double z=d[k]*n[k]+s2i; //actually there is a subtle difference between actual distance and refractive index: distance widens the beam, whereas refractive index only changes the global phase (when actual refraction is not taken into account)
				double r=sqrt(pow(z,2) + pow(x,2) + pow(y,2));
				//todo: apply angular intensity modulation to simulate beam
				E[0] += cos(2*PI*r/wavelength);
				E[1] += sin(2*PI*r/wavelength);
			}
			intensity[i].push_back(pow(E[0],2) + pow(E[1],2));
		}
	}
}

void MZInterference::setD0(double in)
{
	d[0] = in;
	generateIntensity();
	refresh();
}
void MZInterference::setN0(double in)
{
	n[0] = in;
	generateIntensity();
	refresh();
}
void MZInterference::setD1(double in)
{
	d[1] = in;
	generateIntensity();
	refresh();
}
void MZInterference::setN1(double in)
{
	n[1] = in;
	generateIntensity();
	refresh();
}
