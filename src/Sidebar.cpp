#include "Sidebar.h"

Sidebar::Sidebar(QWidget* parent) : QWidget(parent)
{
	vlayout = new QVBoxLayout(this);
	this->setLayout(vlayout);
	vlayout->setContentsMargins(2,2,2,2);
	vlayout->setSpacing(20);
	
	d0 = new QController(this, "d0");
	vlayout->addWidget(d0);

	n0 = new QController(this, "n0");
	vlayout->addWidget(n0);

	d1 = new QController(this, "d1");
	vlayout->addWidget(d1);

	n1 = new QController(this, "n1");
	vlayout->addWidget(n1);

	QWidget* spacer = new QWidget(this);
	spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	vlayout->addWidget(spacer);

	////Colors for testing
	//this->setAutoFillBackground(true);
	//QPalette pal;
	//pal.setColor(QPalette::Background, Qt::green);
	//this->setPalette(pal);
}

QSize Sidebar::sizeHint() const
{
	return QSize(300,300);
}

QController* Sidebar::getD0()
{
	return d0; 
}
QController* Sidebar::getN0()
{
	return n0; 
}
QController* Sidebar::getD1()
{
	return d1; 
}
QController* Sidebar::getN1()
{
	return n1; 
}
