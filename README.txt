This project gives a visualization of the interference pattern that results in a mach-zehnder-type interferometer when the path lengths vary. For now only the path lengths itself and the refractive indices of those paths can be changed. Also the resulting images are for a point source of light.

This project is largely unfinished and would ideally simulate gaussian beams as well. Additionally it would be great to be able to offset the beams and even change their angular alignment.
